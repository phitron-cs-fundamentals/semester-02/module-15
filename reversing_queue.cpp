#include <bits/stdc++.h>
using namespace std;
//15.5:reversing queue from codingNinja

int main()
{   
    queue<int> reverseQueue(queue<int> q)
    {
        stack<int>s;//start from here
        while (!q.empty())
        {
            s.push(q.front());
            q.pop();
        }
        while (!s.empty())
        {
            q.push(s.top());
            s.pop();
        }
        return q;  //end here      
    }
    return 0;
}