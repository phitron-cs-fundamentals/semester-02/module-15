#include <bits/stdc++.h>
// from the Leetcode
using namespace std;
class myStack
{
public:
    bool isValid(string s)
    {
        stack<char>st;
        for(char c:s)
        {
            if(c=='(' || c=='{' || c=='[')
            {
                st.push(c);
            }
            else
            {
                if(st.empty())
                {
                    return false;
                }
                else
                {
                    if(c==')' && st.top()=='(')
                    {
                        st.pop();
                    }
                    else if(c=='}' && st.top()=='{')
                    {
                        st.pop();
                    }
                    else if(c==']' && st.top()=='[')
                    {
                        st.pop();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        return st.empty();
    }
};
int main()
{
    myStack();
    return 0;
}