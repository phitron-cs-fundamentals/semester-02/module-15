#include<bits/stdc++.h>
//15.3 Insert Element at Bottom of Stack from @Codingninjas

using namespace std;
int main()
{
    stack<int> pushAtBottom(stack<int>& st, int x) //Start here
    {
        stack<int> newSt; //main code start here
        while (!st.empty())
        {
            newSt.push(st.top());
            st.pop();
        }
        newSt.push(x);
        while (!newSt.empty())
        {
            st.push(newSt.top());
            newSt.pop();
        }        
        return st; //end code    
    } 
    return 0;
}